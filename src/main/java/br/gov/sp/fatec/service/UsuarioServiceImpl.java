package br.gov.sp.fatec.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.gov.sp.fatec.model.Filme;
import br.gov.sp.fatec.model.Usuario;
import br.gov.sp.fatec.repository.UsuarioRepository;

@Service("usuarioService")
public class UsuarioServiceImpl implements UsuarioService {
	
	@Autowired
	private UsuarioRepository usuarioRepo;

	
	public List<Usuario> buscar(String nome) {
		return usuarioRepo.findByNomeContainsIgnoreCase(nome);
	}
	
	
	public Usuario buscar(Long id) {
		return usuarioRepo.findOne(id);
	}


	public List<Usuario> todos() {
		List<Usuario> retorno = new ArrayList<Usuario>();
		for(Usuario usuario: usuarioRepo.findAll()) {
			retorno.add(usuario);
		}
		return retorno;
	}
	
	public List<Filme> filmes(Long idUsuario) {
		Usuario usuario = usuarioRepo.findOne(idUsuario);
		List<Filme> filmes = usuario.getFilmes();
		
		return filmes;
	}
	
	
	public Usuario salvar(Usuario usuario) {
		return usuarioRepo.save(usuario);
	}


	public UsuarioRepository getUsuarioRepo() {
		return usuarioRepo;
	}

	
	public void setUsuarioRepo(UsuarioRepository usuarioRepo) {
		this.usuarioRepo = usuarioRepo;
	}

}
