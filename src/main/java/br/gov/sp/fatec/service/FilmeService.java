package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Filme;

public interface FilmeService {
	
	public Filme salvar(Filme filme);
	
	public void excluir(Long idFilme);
	
	public List<Filme> todos();
	
	public List<Filme> buscar(String nome);
	
	public Filme buscarPorId(Long idFilme);

}
