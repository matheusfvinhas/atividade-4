package br.gov.sp.fatec.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Filme;
import br.gov.sp.fatec.repository.FilmeRepository;

@Service("filmeService")
@Transactional
public class FilmeServiceImpl implements FilmeService {

	@Autowired
	private FilmeRepository filmeRepo;
	
	public void setFilmeRepo(FilmeRepository filmeRepo) {
		this.filmeRepo = filmeRepo;
	}
	
	
	public Filme salvar(Filme filme) {
		return filmeRepo.save(filme);
	}

	
	public void excluir(Long idFilme) {
		filmeRepo.delete(idFilme);
	}

	
	public List<Filme> todos() {
		List<Filme> retorno = new ArrayList<Filme>();
		for(Filme autorizacao: filmeRepo.findAll()) {
			retorno.add(autorizacao);
		}
		return retorno;
	}

	
	public List<Filme> buscar(String nome) {
		if(nome == null || nome.isEmpty()) {
			return todos();
		}
		return filmeRepo.findByNomeConstainsIgnoreCase(nome);
	}

	
	public Filme buscarPorId(Long idFilme) {
		return filmeRepo.findOne(idFilme);
	}

}

