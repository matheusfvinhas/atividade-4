package br.gov.sp.fatec.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.gov.sp.fatec.model.Filme;

public interface FilmeRepository extends CrudRepository<Filme, Long>{
	
	public List<Filme> findByNomeConstainsIgnoreCase(String nome);
	
	public List<Filme> findByDiretor(String diretor);
	
	public List<Filme> findByGenero(String genero);	
	
	
}
