package br.gov.sp.fatec.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FIL_FILME")
public class Filme {
	
	@Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "FIL_ID")
	private Long id;
    
    @Column(name = "FIL_NOME", unique=true, length = 40, nullable = false)
    private String nome;
    
    @Column(name = "FIL_GENERO", length = 10, nullable = false)
    private String genero;
    
    @Column(name = "FIL_DIRETOR", length = 20, nullable = false)
    private String diretor;
    
    @Column(name = "FIL_ATORES", length = 100, nullable = false)
    private String atores;
    
    @Column(name = "FIL_ROTEIRISTA", length = 100, nullable = false)
    private String roteirista;
    
	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getDiretor() {
		return diretor;
	}

	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}

	public String getAtores() {
		return atores;
	}

	public void setAtores(String atores) {
		this.atores = atores;
	}

	public String getRoteirista() {
		return roteirista;
	}

	public void setRoteirista(String roteirista) {
		this.roteirista = roteirista;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString(){
		return nome;
		
	}

}
