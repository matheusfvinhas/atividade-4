package br.gov.sp.fatec.web.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.sp.fatec.model.Filme;
import br.gov.sp.fatec.service.FilmeService;

@RestController
public class FilmeController {
	
	@Autowired
	private FilmeService filmeService;

	public void setAutorizacaoService(FilmeService filmeService) {
		this.filmeService = filmeService;
	}
	
	@RequestMapping(value = "/")
	public String hello() {
		return "Olá";
	}
	
	@RequestMapping(value = "/filme/get/{nome}")
	public ResponseEntity<Collection<Filme>> pesquisar(@PathVariable("nome") String nome) {		
		return new ResponseEntity<Collection<Filme>>(filmeService.buscar(nome), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/filme/getAll")
	public ResponseEntity<Collection<Filme>> getAll() {
		return new ResponseEntity<Collection<Filme>>(filmeService.todos(), HttpStatus.OK);
	}
	
}